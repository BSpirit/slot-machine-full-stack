# Mindful slot-machine

## Prerequisites

You need the following software to start using the project:

- Node.js (v12+) & npm
- Angular CLI (v8)  
  Install instructions:
  > `npm i -g @angular/cli@8.3.26`

You could need the following software:

- An HTTP server, one can be used with the following commands:  
  > `npm i -h http-server`  
  > `http-server --port <X>`

## Start the project for development

1. Clone the repo
2. At the root of the project, run the following command:  
  `npm i`
3. Then serve the app:  
  `ng serve --host 0.0.0.0 --disable-host-check --port <X>`
4. The app should be available at `http://<ip>:<X>/` (ex: `http://localhost:4200/`)

Beware other app components must be cloned, initiated and started for the app to work. Have a look at the following components:

- Backend server (+ database)
- Backend dashboard

## Build instructions

1. `npm i`
2. `ng build --prod`
3. The complete build should now be available at: `./dist/slot-machine`

You can serve the build in an HTTP server.
