import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Category, News, Source, SubCategory, UserData, UsersTimeSpent} from './backend-objects';
import {User} from './user';
import {Game} from './game';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private static readonly SERVER_URL = `http://${location.hostname}:5000`;

  constructor(private http: HttpClient) {
    console.log(this);
  }

  public async getSources(): Promise<Source[]> {
    return await this.http.get(`${BackendService.SERVER_URL}/sources`).toPromise() as Source[];
    // return [{
    //   icon_id: 'mdi-newspaper',
    //   id: 1,
    //   name: 'News'
    // }];
  }

  public async getCategories(): Promise<Category[]> {
    return await this.http.get(`${BackendService.SERVER_URL}/categories`).toPromise() as Category[];
  }

  public async getAllSubCategories(): Promise<SubCategory[]> {
    return await this.http.get(`${BackendService.SERVER_URL}/subcategories`).toPromise() as SubCategory[];
  }

  public async getNews(id: number): Promise<News> {
    const p: HttpParams = new HttpParams().set('id', id.toString());
    return (await this.http.get(`${BackendService.SERVER_URL}/news`, { params: p }).toPromise())[0] as News;
  }

  public async getSubCategories(categoryId: number): Promise<SubCategory[]> {
    return await this.http.get(`${BackendService.SERVER_URL}/subcategories`, {
      params: new HttpParams().set('category_id', categoryId.toString())
    }).toPromise() as SubCategory[];
  }

  public async getRandomNews(sourceId: number, categoryId: number, subCategoryId: number): Promise<News> {
    const p: HttpParams = new HttpParams()
      .set('source_id', sourceId.toString())
      .set('category_id', categoryId.toString())
      .set('subcategory_id', subCategoryId.toString());

    return await this.http.get(`${BackendService.SERVER_URL}/random-news`, { params: p }).toPromise() as News;
  }

  public async postUser(user: User): Promise<User> {
    return await this.http.post(`${BackendService.SERVER_URL}/users`, user).toPromise() as User;
  }

  public async postGame(game: Game) {
    return this.http.post(`${BackendService.SERVER_URL}/games`, game).toPromise();
  }

  public async getOverallTS(sex?: 'F' | 'M' | 'O', minAge?: number, maxAge?: number): Promise<UsersTimeSpent> {
    let p: HttpParams = new HttpParams();
    if (sex) {
      p = p.append('sex', sex);
    }
    if (minAge) {
      p = p.append('min_age', minAge.toString());
    }
    if (maxAge) {
      p = p.append('max_age', maxAge.toString());
    }

    return await this.http.get(`${BackendService.SERVER_URL}/users/average-time-spent`, { params: p }).toPromise() as UsersTimeSpent;
  }

  public async getUserData(user: User): Promise<UserData> {
    return await this.http.get(`${BackendService.SERVER_URL}/users/${user.id}/infos`).toPromise() as UserData;
  }

  public async getGames(user: User): Promise<Game[]> {
    const p: HttpParams = new HttpParams().set('user_id', user.id.toString());
    return await this.http.get(`${BackendService.SERVER_URL}/games`, { params: p }).toPromise() as Game[];
  }

}
