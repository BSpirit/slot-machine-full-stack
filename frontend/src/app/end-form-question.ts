import {User} from './user';

export interface EndFormQuestion {
    question: string;
    inputType: EndFormInputType;
    inputLabel: string;
    inputResponse?: unknown;
    getResultSentence: (user: User, value: unknown) => Promise<string>;
    resultSentence?: string;
    getCorrectAnswer: (user: User, value: unknown) => Promise<boolean>;
    correctAnswer?: boolean;
}

export enum EndFormInputType {
    TIME= 'time',
    NUMBER = 'number',
    YES_NO = 'yes_no'
}
