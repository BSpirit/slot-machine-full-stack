export interface BarEntry {
  name: string;
  icon_id: string;
}

export interface Source extends BarEntry {
  id: number;
}

export interface Category extends BarEntry {
  id: number;
  color: string;
}

export interface SubCategory extends BarEntry {
  id: number;
  category_id;
}

export interface News {
  id?: number;
  url: string;
  category_id: number;
}

export interface UsersTimeSpent {
  average_time_spent: number;
}

export interface UserData {
  total_news: number;
  total_time: number;
  user_id: number;
}
