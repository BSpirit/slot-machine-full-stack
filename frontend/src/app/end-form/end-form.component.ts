import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {NgForm} from '@angular/forms';
import {EndFormInputType, EndFormQuestion} from '../end-form-question';

@Component({
  selector: 'app-end-form',
  templateUrl: './end-form.component.html',
  styleUrls: ['./end-form.component.scss']
})
export class EndFormComponent implements OnInit {

  constructor(public user: UserService) { }

  public display = true;

  public get currentQuestion(): EndFormQuestion {
    return this.user.questions[this.user.currentQuestionIndex];
  }

  ngOnInit() {

  }

  async onSubmit(form: NgForm) {

    // Sanitize value type
    switch (this.currentQuestion.inputType) {
      case EndFormInputType.NUMBER:
      case EndFormInputType.TIME:
        this.currentQuestion.inputResponse = Number(form.value.value);
        break;
      default:
        this.currentQuestion.inputResponse = form.value.value;

    }

    // Check end of questions
    if (this.user.currentQuestionIndex + 1 >= this.user.questions.length) {
      await this.user.endUserForm();
      // Stop if last reached
      this.display = false;
      console.log(this.user.questions);
      return;
    } else {
      // Go on if more questions to come
      this.user.currentQuestionIndex++;
      form.reset();
    }
  }


}
