import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {User} from '../user';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  public model: User;

  constructor(public user: UserService) { }

  ngOnInit() {
    this.model = this.user.user;
  }

  onSubmit(form: NgForm) {
    this.user.init(this.model);
  }

}
