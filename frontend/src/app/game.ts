export interface Game {
  id?: number;
  news_id: number;
  user_id: number;
  passed_time: number;
  liked: number;
}
