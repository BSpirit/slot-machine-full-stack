import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import * as $ from 'jquery';
import {BackendService} from '../backend.service';
import {SpinResult} from '../spin-result';
import {Category, Source, SubCategory} from '../backend-objects';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit {

  public sourceEntries: Source[] = [];
  public categoryEntries: Category[] = [];
  private subCategoryEntries: SubCategory[] = [];
  public filterSubCategoryEntries: SubCategory[] = [];

  private currentSourceIndex = 0;
  private currentCategoryIndex = 0;
  private currentSubCategoryIndex = 0;
  private spinning = false;

  @Output()
  private spinResult = new EventEmitter<SpinResult>();

  @ViewChild('sourceBar', { static: true }) sourceBar;
  @ViewChild('categoryBar', { static: true }) categoryBar;
  @ViewChild('objectBar', { static: true }) subCategoryBar;
  @ViewChild('spinButton', { static: true }) spinButton;

  constructor(private barElementsService: BackendService) { }

  ngOnInit() {
    (async () => {
      this.sourceEntries = await this.barElementsService.getSources();
      this.categoryEntries = await this.barElementsService.getCategories();
      this.subCategoryEntries = await this.barElementsService.getAllSubCategories();
      this.filterSubCategoryEntries = this.subCategoryEntries;
    })();
  }

  public async spinAll(): Promise<void> {
    if (!this.spinning) {
      this.filterSubCategoryEntries = this.subCategoryEntries;
      this.subCategoryBar.barEntries = this.filterSubCategoryEntries;

      this.spinning = true;
      this._pauseAnimations();
      const promises: Promise<number>[] = [];
      promises.push(this.sourceBar.spin((i: number) => this.currentSourceIndex = i, 350));
      promises.push(this.categoryBar.spin((i: number) => {
        this.currentCategoryIndex = i;
      }, 450).then((i: number) => {
        this.currentCategoryIndex = i;
        this.filterSubCategoryEntries = this.subCategoryEntries.filter((sc: SubCategory) => sc.category_id === this.categoryEntries[i].id);
        this.subCategoryBar.barEntries = this.filterSubCategoryEntries;
      }));
      promises.push(this.subCategoryBar.spin((i: number) => this.currentSubCategoryIndex = i, 600));
      await Promise.all(promises);
      this._resumeAnimations();
      this.spinning = false;
      this._emitSpinResult();
    }
  }

  private _delay<T>(c: () => T, delay: number): Promise<T> {
    return new Promise((resolve: (result: T) => void, reject: (reason: any) => void) => {
      setTimeout(async () => {
        try {
          resolve(await c());
        } catch (e) {
          reject(e);
        }
      }, delay);
    });
  }

  private _pauseAnimations(): void {
    this.spinButton.nativeElement.style.animationPlayState = 'paused';
    // this.spinButton.nativeElement.style.transform = 'scale(0.25)';
    this.spinButton.nativeElement.style.display = 'none';
    this.spinButton.nativeElement.style.filter = 'saturate(0%)';
  }

  private _resumeAnimations(): void {
    // this.spinButton.nativeElement.style.transform = 'scale(1)';
    this.spinButton.nativeElement.style.display = 'block';
    this.spinButton.nativeElement.style.animationPlayState = 'running';
    this.spinButton.nativeElement.style.filter = '';
  }

  private _emitSpinResult(): void {
    this.spinResult.emit({
      source: this.sourceEntries[this.currentSourceIndex],
      category: this.categoryEntries[this.currentCategoryIndex],
      subCategory: this.filterSubCategoryEntries[this.currentSubCategoryIndex],
    });
  }

}
