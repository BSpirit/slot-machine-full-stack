import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {SpinResult} from '../spin-result';
import {News} from '../backend-objects';
import {BackendService} from '../backend.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-content-popup',
  templateUrl: './content-popup.component.html',
  styleUrls: ['./content-popup.component.scss']
})
export class ContentPopupComponent implements OnInit/*, ContentComponent*/ {

  @ViewChild('wrapper', { static: true }) wrapper;
  @ViewChild('article', { static: true }) article;
  @ViewChild('content', { static: false }) content;

  @Input()
  public spinResult: SpinResult;

  @Input()
  public news: News;

  constructor(private user: UserService) { }

  ngOnInit() {
    this.spinResult = {
      source: null,
      category: null,
      subCategory: null,
    };

    // (async () => {
    //   this.spinResult = {
    //     source: { id: 1, name: "News", icon_id: '' },
    //     category: { id: 8, name: "culture", icon_id: '', color: '' },
    //     subCategory: { id: 15, name: "food", icon_id: '', category_id: 8 },
    //   };
      // this.news = await this.backend.getRandomNews(this.spinResult.source.id, this.spinResult.category.id, this.spinResult.subCategory.id);
      // console.log(this.news);
    // })();
  }

  public async pop(): Promise<void> {
    this.wrapper.nativeElement.style.zIndex = '2000';
    this.wrapper.nativeElement.style.opacity = '0';
    this.wrapper.nativeElement.style.opacity = '1';

    console.log(this.news);
  }

  public async leave() {
    if (this.user.isWatching) {
      await this.user.stopTiming();
    }
    this.user.endForm = true;
    await this.out();
  }

  public async out(): Promise<void> {
    if (this.user.isWatching) {
      await this.user.stopTiming();
    }
    this.spinResult = {
      source: null,
      category: null,
      subCategory: null,
    };
    this.news = null;
    this.wrapper.nativeElement.style.opacity = '0';
    setTimeout(() => this.wrapper.nativeElement.style.zIndex = '-1', 500);
  }

}
