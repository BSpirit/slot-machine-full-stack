import {AfterViewInit, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.sass']
})
export class TwitterComponent implements OnInit, AfterViewInit {

  @Input()
  private url: string;

  constructor() { }

  ngOnInit() {
    // @ts-ignore
    twttr.widgets.createTweet(this._getId(), document.getElementById('tweet-container'), {
      align: 'center',
    });
  }

  ngAfterViewInit(): void {
    // @ts-ignore
    // twttr.widgets.load();
  }

  private _getId(): string {
    if (!this.url) {
      throw new Error('No URL given!');
    }

    // return this.url.match(/(?<id>\d+)$/).groups.id;
    return this.url.match(/(\d+)$/)[1];
  }

}
