import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-youtube',
  templateUrl: './youtube.component.html',
  styleUrls: ['./youtube.component.scss']
})
export class YoutubeComponent implements OnInit {

  @Input()
  private url: string;

  public get youtubeLink(): string {
    return `https://www.youtube-nocookie.com/embed/${this.url.match(/v=(.+)$/)[1]}`;
  }

  constructor() { }

  ngOnInit() {
  }

}
