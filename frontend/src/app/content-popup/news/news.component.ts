import {Component, Input, OnInit, SecurityContext} from '@angular/core';
import {News} from '../../backend-objects';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  @Input()
  public news: News;

  constructor() { }

  ngOnInit() {
  }

}
