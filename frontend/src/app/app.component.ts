import {Component, ViewChild} from '@angular/core';
import {SpinResult} from './spin-result';
import {BackendService} from './backend.service';
import {News} from './backend-objects';
import {UserService} from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'slot-machine';

  public currentResult: SpinResult;

  @ViewChild('contentPopup', { static: true }) contentPopup;

  constructor(public backend: BackendService, public user: UserService) {
  }

  public async onSpinResult($event: SpinResult): Promise<void> {
    this.currentResult = $event;
    this.contentPopup.spinResult = $event;
    try {
      const news: News = await this.backend.getRandomNews($event.source.id, $event.category.id, $event.subCategory.id);
      this.contentPopup.news = news;
      console.log(news);
      this.contentPopup.pop();
      this.user.startTiming(news);
    } catch (e) {
      alert('Error getting random news [see console]');
      console.error(e);
    }
  }
}
