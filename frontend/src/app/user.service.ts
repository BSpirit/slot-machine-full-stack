import {Injectable} from '@angular/core';
import {User} from './user';
import {BackendService} from './backend.service';
import {Category, News, UserData, UsersTimeSpent} from './backend-objects';
import {EndFormInputType, EndFormQuestion} from './end-form-question';
import {Game} from './game';
import {ChartData, ChartDataSets, RadialChartOptions} from 'chart.js';
import {Label} from 'ng2-charts';
import * as _ from 'lodash';

interface EndFormChart {
  datasets: ChartDataSets[];
  labels: Label[];
  options: RadialChartOptions;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public get user(): User {
    return this.user_;
  }

  public get hasUser(): boolean {
    return this.user_ && !!this.user_.age && !!this.user_.sexe;
  }

  public get isWatching(): boolean {
    return !!this.currentNews && !!this.startTime;
  }

  constructor(private backend: BackendService) {
    console.log(this);
  }

  private startTime: Date;
  private currentNews: News;

  private user_: User = {
    id: null,
    sexe: null,
    age: null
  };

  public submitted = false;




  public endForm = false;

  // tslint:disable:max-line-length
  public questions: EndFormQuestion[] = [
    {
      question: 'How much time do you think you spent playing?',
      inputType: EndFormInputType.NUMBER,
      inputLabel: 'Time (minutes)',
      inputResponse: null,
      getCorrectAnswer: async (u: User, response: number) => {
        const userData: UserData = await this.gatherUserData();
        return response >= userData.total_time / 60;
      },
      getResultSentence: async (u: User, response: number) => {
        const userData: UserData = await this.gatherUserData();
        userData.total_time = this._secondsToMinutes(userData.total_time);
        if (response > userData.total_time) {
          return `Actually, you have spent <strong>less time</strong> playing than you thought! You said you played for <strong>${response} minutes</strong>, and you actually spent <strong>${userData.total_time} minutes</strong> playing.`;
        } else if (response < userData.total_time) {
          return `Actually, you have spent <strong>more time</strong> playing than you thought! You said you played for <strong>${response} minutes</strong>, and you actually spent <strong>${userData.total_time} minutes</strong> playing.`;
        } else {
          return `You got it right! You spent ${response} minutes playing!`;
        }
      }
    },
    {
      question: 'Do you think you spent less time playing than other players of your age?',
      inputType: EndFormInputType.YES_NO,
      inputLabel: '',
      inputResponse: true,
      getCorrectAnswer: async (u: User, response: boolean) => {
        const userData: UserData = await this.gatherUserData();
        const averages: UsersTimeSpent = await this.backend.getOverallTS(undefined, this.user.age, this.user.age);
        return response === (userData.total_time <= averages.average_time_spent);
      },
      getResultSentence: async (u: User, response: boolean) => {
        const userData: UserData = await this.gatherUserData();
        const averages: UsersTimeSpent = await this.backend.getOverallTS(undefined, this.user.age, this.user.age);
        const correctAnswer: boolean = response === (userData.total_time <= averages.average_time_spent);
        const right: boolean = userData.total_time <= averages.average_time_spent;

        const answerString: string = correctAnswer ? 'You are right!' : 'You got the wrong answer!';
        return `${answerString} You actually spent <strong>${this._secondsToMinutes(userData.total_time)} minutes</strong> playing while other players of your age spent <strong>${this._secondsToMinutes(averages.average_time_spent)} minutes</strong> on average.`;
      }
    },
    {
      question: 'Do you think you spent less time playing than other players of your sex?',
      inputType: EndFormInputType.YES_NO,
      inputLabel: '',
      inputResponse: true,
      getCorrectAnswer: async (u: User, response: boolean) => {
        const userData: UserData = await this.gatherUserData();
        const averages: UsersTimeSpent = await this.backend.getOverallTS(this.user.sexe);
        return response === (userData.total_time <= averages.average_time_spent);
      },
      getResultSentence: async (u: User, response: boolean) => {
        const userData: UserData = await this.gatherUserData();
        const averages: UsersTimeSpent = await this.backend.getOverallTS(this.user.sexe);
        const correctAnswer: boolean = response === (userData.total_time <= averages.average_time_spent);
        const right: boolean = userData.total_time <= averages.average_time_spent;

        const answerString: string = correctAnswer ? 'You are right!' : 'You got the wrong answer!';
        return `${answerString} You actually spent <strong>${this._secondsToMinutes(userData.total_time)} minutes</strong> playing while other players of your age spent <strong>${this._secondsToMinutes(averages.average_time_spent)} minutes</strong> on average.`;
      }
    },
    {
      question: 'How many spins do you think you made?',
      inputType: EndFormInputType.NUMBER,
      inputLabel: 'Number of times',
      inputResponse: null,
      getCorrectAnswer: async (u: User, response: number) => {
        const userData: UserData = await this.gatherUserData();
        return response >= userData.total_news;
      },
      getResultSentence: async (u: User, response: number) => {
        const userData: UserData = await this.gatherUserData();
        return `You said you spinned <strong>${response} times </strong> and you actually spinned <strong>${userData.total_news} times</strong>.`;
      }
    },
  ];
  // tslint:enable:max-line-length

  public currentQuestionIndex = 0;

  public games: Game[] = null;

  public categories: Category[] = [];

  public userData: UserData = null;

  public chart: EndFormChart = null;

  public async init(user: User): Promise<void> {
    this.user_ = await this.backend.postUser(user);
    this.submitted = true;
  }

  public startTiming(news: News): void {
    this.startTime = new Date();
    this.currentNews = news;
  }

  public async stopTiming() {
    await this.backend.postGame({
      news_id: this.currentNews.id,
      liked: null,
      user_id: this.user_.id,
      passed_time: Math.abs((new Date().getTime() - this.startTime.getTime()) / 1000)
    });
    this.currentNews = null;
    this.startTime = null;
  }

  public openDashboard() {
    open(`http://${location.hostname}:5000/dashboard`, 'Dashboard', 'resizable,scrollbars,dialog,fullscreen');
  }

  public async endUserForm() {
    for (const question of this.questions) {
      question.resultSentence = await question.getResultSentence(this.user_, question.inputResponse);
      question.correctAnswer = await question.getCorrectAnswer(this.user_, question.inputResponse);
    }

    this.games = await this.backend.getGames(this.user_);
    this.categories = await this.backend.getCategories();

    const CatIdToGames: Map<number, number> = new Map();
    for (const category of this.categories) {
      CatIdToGames.set(category.id, 0);
    }
    for (const game of this.games) {
      const news: News = await this.backend.getNews(game.news_id);
      console.log(game.passed_time);
      CatIdToGames.set(news.category_id, CatIdToGames.get(news.category_id) + Number((game.passed_time / 60).toPrecision(2)));
    }

    this.chart = {
      labels: this.categories.map(c => _.capitalize(c.name)),
      datasets: [
        {
          data: Array.from(CatIdToGames.values()),
          label: 'You',
          backgroundColor: 'rgba(3,169,244,0.65)',
          borderColor: '#0288d1',
          pointBackgroundColor: '#4fc3f7'
        }
      ],
      options: {
        responsive: true,
        legend: {
          labels: {
            fontColor: '#ffffff'
          }
        },
        scale: {
          angleLines: {
            color: '#b6b6b6'
          },
          gridLines: {
            display: true,
            color: '#b6b6b6'
          },
          ticks: {
            // fontColor: '#ffffff'
          },
          pointLabels: {
            fontColor: '#ffffff'
          }
        }
      }
    };
  }

  public async gatherUserData() {
    if (!this.userData) {
      this.userData = await this.backend.getUserData(this.user);
    }

    return this.userData;
  }

  private _secondsToMinutes(seconds: number): number {
    return Number((seconds / 60).toPrecision(1));
  }

  public leaveGame() {
    this.openDashboard();
    setTimeout(() => {
      location.reload();
    }, 500);
  }
}
