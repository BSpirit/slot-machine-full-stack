import {Component, Input, OnInit, ViewChild} from '@angular/core';
import * as $ from 'jquery';
import {BarEntry} from '../backend-objects';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss']
})
export class BarComponent implements OnInit {

  @Input()
  public barEntries: BarEntry[];

  public currentEntryIndex = 0;

  public filterFn: (entry: BarEntry) => boolean;

  @ViewChild('barContainer', { static: true }) barContainer;

  constructor() {
  }

  ngOnInit() {
    this._scrollTo(this.barContainer.nativeElement.clientHeight, 0);
  }

  public async spin(spinAction?: (index: number) => void, duration: number = 700): Promise<number> {
    const MAX_DURATION = duration;

    // Spin with an increasing duration
    for (let d = 100; d < MAX_DURATION; d += 30) {
      await this._scrollTo(0, 1);
      await this._scrollTo(this.barContainer.nativeElement.clientHeight + this.barContainer.nativeElement.scrollHeight, d);
      ++this.currentEntryIndex > this.barEntries.length - 1 ? this.currentEntryIndex = 0 : null;

      if (spinAction) {
        spinAction(this.currentEntryIndex);
      }
    }

    this.currentEntryIndex = Math.floor(Math.random() * this.barEntries.length);
    if (spinAction) {
      spinAction(this.currentEntryIndex);
    }
    await this._scrollTo(0, 1);
    await this._scrollTo(this.barContainer.nativeElement.clientHeight, MAX_DURATION + 100);
    return this.currentEntryIndex;
  }

  private async _scrollTo(offsetTop: number, duration: number): Promise<void> {
    return new Promise((resolve: () => void, reject: (reason: any) => void) => {
      $(this.barContainer.nativeElement).animate({scrollTop: offsetTop}, duration, () => {
        resolve();
      });
    });
  }

}
