import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { CoreComponent } from './core/core.component';
import { BarComponent } from './bar/bar.component';
import { ContentPopupComponent } from './content-popup/content-popup.component';
import { YoutubeComponent } from './content-popup/youtube/youtube.component';
import { TwitterComponent } from './content-popup/twitter/twitter.component';
import {NgxTweetModule} from 'ngx-tweet';
import { NewsComponent } from './content-popup/news/news.component';
import { ImageComponent } from './content-popup/image/image.component';
import {HttpClientModule} from '@angular/common/http';
import { SafePipe } from './safe.pipe';
import { UserFormComponent } from './user-form/user-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import { EndFormComponent } from './end-form/end-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CoreComponent,
    BarComponent,
    ContentPopupComponent,
    YoutubeComponent,
    TwitterComponent,
    NewsComponent,
    ImageComponent,
    SafePipe,
    UserFormComponent,
    EndFormComponent
  ],
  imports: [
    BrowserModule,
    NgxTweetModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
