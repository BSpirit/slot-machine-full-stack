export interface User {
  id?: number;
  sexe: 'F' | 'M' | 'O';
  age: 18 | 25 | 35 | 45 | 55 | 65;
}
