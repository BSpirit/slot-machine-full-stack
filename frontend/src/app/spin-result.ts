import {Category, Source, SubCategory} from './backend-objects';

export interface SpinResult {
  source: Source;
  category: Category;
  subCategory: SubCategory;
}
