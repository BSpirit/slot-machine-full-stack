# slot-machine


## Requirements

We decided to use Docker and Docker compose to make the slot-machine application runnable on any operating system.

1. Install Docker
https://docs.docker.com/get-docker/

2. Install Docker compose
https://docs.docker.com/compose/install/

**Be sure to close every running terminal before doing the following actions.**

In order to run the application you have to use the following commands in a terminal. The current working directory of the terminal must be the “code_source” directory. The directory called “code_source” is the root directory of the project.

## Run application

1. Build the application
```
docker-compose build
```

2. Run the application
```
docker-compose up
```

## Access application

The application is accessible using any internet browser, such as Mozilla Firefox or Google Chrome.

Once the application is running, the slot machine can be accessed through http://localhost:4200/.
