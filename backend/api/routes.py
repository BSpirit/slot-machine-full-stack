from app import server, db
from api.models.news import News
from flask import request
import random


@server.route("/random-news", methods=['GET'])
def randow_news():
    q = db.session.query(News)

    for key, value in request.args.items():
        try:
            q = q.filter(getattr(News, key) == value)
        except AttributeError:
            pass

    nb_rows = q.count()
    elt, code = {}, 404
    if nb_rows > 0:
        rand = random.randrange(0, nb_rows)
        elt, code = q[rand].to_dict(), 200

    return elt, code
