from flask_restful import reqparse
from api.resources.base import GenericListAPI, GenericAPI
from api.models.subcategory import Subcategory


subcategory_parser = reqparse.RequestParser()
subcategory_parser.add_argument('name', type=str, required=True, location='json')
subcategory_parser.add_argument('icon_id', type=str, location='json')
subcategory_parser.add_argument('category_id', type=int, required=True, location='json')


class SubcategoryListAPI(GenericListAPI):
    model = Subcategory
    parser = subcategory_parser


class SubcategoryAPI(GenericAPI):
    model = Subcategory
    parser = subcategory_parser