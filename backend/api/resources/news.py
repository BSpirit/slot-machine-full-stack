from flask_restful import reqparse
from api.resources.base import GenericListAPI, GenericAPI
from api.models.news import News


news_parser = reqparse.RequestParser()
news_parser.add_argument('title', type=str, required=True, location='json')
news_parser.add_argument('url', type=str, required=True, location='json')
news_parser.add_argument('url_image', type=str, location='json')
news_parser.add_argument('content', type=str, required=True, location='json')
news_parser.add_argument('source_id', type=int, required=True, location='json')
news_parser.add_argument('category_id', type=int, required=True, location='json')
news_parser.add_argument('subcategory_id', type=int, required=True, location='json')
news_parser.add_argument('type_id', type=int, required=True, location='json')
news_parser.add_argument('author', type=str, location='json')
news_parser.add_argument('fake', type=int, location='json')


class NewsListAPI(GenericListAPI):
    model = News
    parser = news_parser


class NewsAPI(GenericAPI):
    model = News
    parser = news_parser
