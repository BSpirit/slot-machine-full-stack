from flask_restful import reqparse
from api.resources.base import GenericListAPI, GenericAPI
from api.models.category import Category


category_parser = reqparse.RequestParser()
category_parser.add_argument('name', type=str, required=True, location='json')
category_parser.add_argument('icon_id', type=str, location='json')
category_parser.add_argument('color', type=str, location='json')


class CategoryListAPI(GenericListAPI):
    model = Category
    parser = category_parser


class CategoryAPI(GenericAPI):
    model = Category
    parser = category_parser
