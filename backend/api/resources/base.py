from flask import abort, request
from flask_restful import Resource
from app import db


class GenericListAPI(Resource):
    model = None
    parser = None

    def get(self):
        q = db.session.query(self.model)
        for key, value in request.args.items():
            try:
                q = q.filter(getattr(self.model, key) == value)
            except AttributeError:
                pass
        return [resource.to_dict() for resource in q]

    def post(self):
        kwargs = self.parser.parse_args()
        resource = self.model(**kwargs)
        db.session.add(resource)
        db.session.commit()
        return resource.to_dict(), 201


class GenericAPI(Resource):
    model = None
    parser = None

    def get(self, id):
        resource = db.session.query(self.model).get(id)
        if not resource:
            return abort(404)
        return resource.to_dict()

    def put(self, id):
        resource = db.session.query(self.model).get(id)
        if not resource:
            return abort(404)
        kwargs = self.parser.parse_args()
        for key, value in kwargs.items():
            setattr(resource, key, value)
        db.session.commit()
        return "", 200

    def delete(self, id):
        resource = db.session.query(self.model).get(id)
        if not resource:
            return abort(404)
        db.session.delete(resource)
        db.session.commit()
        return "", 200
