from app import db, server
from flask_restful import reqparse, request
from api.resources.base import GenericListAPI, GenericAPI
from api.models.user import User
from api.models.game import Game
from sqlalchemy.sql import func

user_parser = reqparse.RequestParser()
user_parser.add_argument('sexe', type=str, required=True, location='json')
user_parser.add_argument('age', type=int, required=True, location='json')


class UserListAPI(GenericListAPI):
    model = User
    parser = user_parser


class UserAPI(GenericAPI):
    model = User
    parser = user_parser


@server.route("/users/average-time-spent", methods=['GET'])
def users_average_time_spent():
    min_age = request.args.get('min_age')
    max_age = request.args.get('max_age')
    sex = request.args.get('sex')
    
    sub_q = db.session.query(func.sum(Game.passed_time).label("total_time_spent")) \
        .join(User, User.id == Game.user_id)

    if min_age is not None:
        sub_q = sub_q.filter(User.age >= min_age)

    if max_age is not None:
        sub_q = sub_q.filter(User.age <= max_age)

    if sex is not None:
        sub_q = sub_q.filter(User.sexe == sex)

    sub_q = sub_q.group_by(Game.user_id).subquery()
    row = db.session.query(func.avg(sub_q.c.total_time_spent)).first()

    return { "average_time_spent": row[0] if row[0] else 0 }

@server.route("/users/<int:user_id>/infos", methods=['GET'])
def user_infos(user_id):
    row = db.session.query(func.count(Game.id), func.sum(Game.passed_time)) \
        .filter(Game.user_id == user_id) \
        .first()

    return {
        "user_id": user_id,
        "total_time": row[1] if row[1] is not None else 0,
        "total_news": row[0]
        }
