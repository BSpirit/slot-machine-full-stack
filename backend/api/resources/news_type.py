from flask_restful import reqparse
from api.resources.base import GenericListAPI, GenericAPI
from api.models.news_type import NewsType


news_type_parser = reqparse.RequestParser()
news_type_parser.add_argument('name', type=str, required=True, location='json')


class NewsTypeListAPI(GenericListAPI):
    model = NewsType
    parser = news_type_parser


class NewsTypeAPI(GenericAPI):
    model = NewsType
    parser = news_type_parser