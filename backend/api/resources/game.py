from flask_restful import reqparse
from api.resources.base import GenericListAPI, GenericAPI
from api.models.game import Game


game_parser = reqparse.RequestParser()
game_parser.add_argument('news_id', type=int, required=True, location='json')
game_parser.add_argument('user_id', type=int, required=True, location='json')
game_parser.add_argument('passed_time', type=int, location='json')
game_parser.add_argument('liked', type=int, location='json')


class GameListAPI(GenericListAPI):
    model = Game
    parser = game_parser


class GameAPI(GenericAPI):
    model = Game
    parser = game_parser