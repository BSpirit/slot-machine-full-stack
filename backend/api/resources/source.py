from flask_restful import reqparse
from api.resources.base import GenericListAPI, GenericAPI
from api.models.source import Source


source_parser = reqparse.RequestParser()
source_parser.add_argument('name', type=str, required=True, location='json')
source_parser.add_argument('icon_id', type=str, location='json')


class SourceListAPI(GenericListAPI):
    model = Source
    parser = source_parser


class SourceAPI(GenericAPI):
    model = Source
    parser = source_parser
