# API

## Endpoints 

<details><summary>Sources</summary>

1. <details><summary>Get list of sources</summary>

    `/sources`

    No parameters

    Success 200 (list)

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |
    | icon_id     | int    |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

2. <details><summary>Get one source</summary>

    `/sources/:id`

    | Fields      | Type   | Required |
    |----------   |:------:| :------: |
    | id          | int    |   yes    |

    Success 200

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |
    | icon_id     | int    |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

</details>
<details><summary> Categories</summary>

1. <details><summary>Get list of categories</summary>

    `/categories`

    No parameters

    Success 200 (list)

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |
    | icon_id     | int    |
    | color       | string |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

2. <details><summary>Get one category</summary>

    `/categories/:id`

    | Fields      | Type   | Required |
    |----------   |:------:| :------: |
    | id          | int    |   yes    |

    Success 200

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |
    | icon_id     | int    |
    | color       | string |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

</details>

<details><summary> Subcategories</summary>

1. <details><summary>Get list of subcategories</summary>

    `/subcategories`

    No parameters

    Success 200 (list)

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |
    | icon_id     | int    |
    | category_id | string |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

2. <details><summary>Get one subcategory</summary>

    `/subcategories/:id`

    | Fields      | Type   | Required |
    |----------   |:------:| :------: |
    | id          | int    |   yes    |

    Success 200

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |
    | icon_id     | int    |
    | category_id | string |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

</details>

<details><summary> New_types</summary>

1. <details><summary> Get list of new types</summary>

    `/news-types`

    No parameters

    Success 200 (list)

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

2. <details><summary>Get one news type</summary>

    `/news-types/:id`

    | Fields      | Type   | Required |
    |----------   |:------:| :------: |
    | id          | int    |   yes    |

    Success 200

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | name        | string |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

</details>

<details><summary> News</summary>

1. <details><summary>Get list of news</summary>

    `/news`

    No parameters

    Success 200 (list)

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | title       | string |
    | url         | string |
    | url_image   | string |
    | content     | string |
    | source_id   | int    |
    | category_id | int    |
    | subcategory_id | int |
    | type_id     | int    |
    | author      | string |
    | fake        | int    |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

2. <details><summary>Get one news</summary>

    `/news/:id`

    | Fields      | Type   | Required |
    |----------   |:------:| :------: |
    | id          | int    |   yes    |

    Success 200

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | title       | string |
    | url         | string |
    | url_image   | string |
    | content     | string |
    | source_id   | int    |
    | category_id | int    |
    | subcategory_id | int |
    | type_id     | int    |
    | author      | string |
    | fake        | int    |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

</details>

<details><summary> Games</summary>

#### 

1. <details><summary>Get list of games</summary>

    `/games`

    No parameters

    Success 200 (list)

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | news_id     | int    |
    | user_id     | int    |
    | passed_time | int    |
    | liked       | int    |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

2. <details><summary>Get one category</summary>

    `/games/:id`

    | Fields      | Type   | Required |
    |----------   |:------:| :------: |
    | id          | int    |   yes    |

    Success 200

    | Fields      | Type   |
    |----------   |:------:|
    | id          | int    |
    | news_id     | int    |
    | user_id     | int    |
    | passed_time | int    |
    | liked       | int    |

    Error 4xx

    | Fields   | Type   | Description |
    |----------|:------:|:----------: |
    |          |        |             |

    </details>

</details>

<details><summary> Random-news</summary>

`/random-news?source=4&category=1&subcategory=3`

| Fields   | Type | Description  | Required |
|----------|:------:|:--------------:| :--------: |
| source | int  | id of wanted source | No |
| category | int  | id of wanted category | No |
| subcategory | int  | id of wanted subcategory | No |

Success 200

| Fields      | Type   | Description  |
|----------   |:------:|:--------------:|
| title       | string |  |
| url         | string |  |
| url_image   | string |  |
| content     | string |  |
| source_id   | int    |  |
| category_id | int    |  |
| subcategory_id | int |  |
| type_id     | int    |  |
| author      | string |  |
| fake        | int    |  |

Error 4xx

| Fields   | Type | Description  | Required |
|----------|:------:|:--------------:| :--------: |
| | | |  |

</details>