from app import db
from api.models.base import BaseModel


class Source(db.Model, BaseModel):
    __tablename__ = 'Source'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    icon_id = db.Column(db.Text)
