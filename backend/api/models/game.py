from app import db
from api.models.base import BaseModel


class Game(db.Model, BaseModel):
    __tablename__ = 'Game'
    id = db.Column(db.Integer, primary_key=True)
    news_id = db.Column(db.Integer, db.ForeignKey("News.id"), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("User.id"), nullable=False)
    passed_time = db.Column(db.Integer)
    liked = db.Column(db.Integer, default=0)
