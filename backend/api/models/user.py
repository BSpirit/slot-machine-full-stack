from app import db
from api.models.base import BaseModel


class User(db.Model, BaseModel):
    __tablename__ = 'User'
    id = db.Column(db.Integer, primary_key=True)
    sexe = db.Column(db.Text, nullable=False)
    age = db.Column(db.Integer, nullable=False)
