from app import db
from api.models.base import BaseModel


class Subcategory(db.Model, BaseModel):
    __tablename__ = 'Subcategory'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    icon_id = db.Column(db.Text)
    category_id = db.Column(db.Integer, db.ForeignKey("Category.id"), nullable=False)
