from app import db
from api.models.base import BaseModel


class News(db.Model, BaseModel):
    __tablename__ = 'News'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    url = db.Column(db.Text, nullable=False)
    url_image = db.Column(db.Text)
    content = db.Column(db.Text, nullable=False)
    source_id = db.Column(db.Integer, db.ForeignKey("Source.id"), nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey("Category.id"), nullable=False)
    subcategory_id = db.Column(db.Integer, db.ForeignKey("Subcategory.id"), nullable=False)
    type_id = db.Column(db.Integer, db.ForeignKey("News_Type.id"), nullable=False)
    author = db.Column(db.Text, default=None)
    fake = db.Column(db.Integer, default=0)
