from app import db
from api.models.base import BaseModel


class NewsType(db.Model, BaseModel):
    __tablename__ = 'News_Type'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
