import requests
import sqlite3

conn = sqlite3.connect("../../slot-machine.db")
db = conn.cursor()

subjects = ["business", "body", "music", "fun", "astronomy"]
categories = [5, 6, 8, 1, 3]

for i in range(0, len(subjects)):
    for _ in range(0, 10):
        response = requests.get("https://source.unsplash.com/1600x900/?" + subjects[i])
        row = {
                "id": None,
                "title": "",
                "url": response.url,
                "url_image": response.url,
                "content": "",
                "source_id": 4,
                "category_id": categories[i],
                "type_id": 2,
                "author": None
                }
        db.execute("INSERT INTO News VALUES (:id, :title, :url, :url_image, :content, :source_id, :category_id, :type_id, :author)", row)

conn.commit()