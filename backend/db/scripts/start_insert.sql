-- SQLite
INSERT INTO `Category` (name) VALUES ('entertainment');
INSERT INTO `Category` (name) VALUES ('health');
INSERT INTO `Category` (name) VALUES ('science');
INSERT INTO `Category` (name) VALUES ('sports');
INSERT INTO `Category` (name) VALUES ('business');
INSERT INTO `Category` (name) VALUES ('soft-porn');
INSERT INTO `Category` (name) VALUES ('personal news');
INSERT INTO `Category` (name) VALUES ('culture');
INSERT INTO `Category` (name) VALUES ('politic');
INSERT INTO `Category` (name) VALUES ('various facts');
INSERT INTO `Category` (name) VALUES ('societal');


-- entertainement : 1
INSERT INTO `Subcategory` (name, category_id) VALUES ('animals', 1);    -- 1
INSERT INTO `Subcategory` (name, category_id) VALUES ('gag', 1);        -- 2
INSERT INTO `Subcategory` (name, category_id) VALUES ('softporn', 1);   -- 3
INSERT INTO `Subcategory` (name, category_id) VALUES ('video game', 1); -- 4
INSERT INTO `Subcategory` (name, category_id) VALUES ('vehicle', 1);    -- 25
-- science : 3
INSERT INTO `Subcategory` (name, category_id) VALUES ('astronomy', 3);  -- 5
INSERT INTO `Subcategory` (name, category_id) VALUES ('climate', 3);    -- 6
INSERT INTO `Subcategory` (name, category_id) VALUES ('it', 3); -- 7
INSERT INTO `Subcategory` (name, category_id) VALUES ('medicine', 3);   -- 8
INSERT INTO `Subcategory` (name, category_id) VALUES ('physics', 3);    -- 9
INSERT INTO `Subcategory` (name, category_id) VALUES ('earth life', 3); --22
-- sport : 4
INSERT INTO `Subcategory` (name, category_id) VALUES ('athletism', 4);  -- 26
INSERT INTO `Subcategory` (name, category_id) VALUES ('balls', 4);      -- 10
INSERT INTO `Subcategory` (name, category_id) VALUES ('fitness', 4);    -- 11
INSERT INTO `Subcategory` (name, category_id) VALUES ('racket', 4);     -- 12
INSERT INTO `Subcategory` (name, category_id) VALUES ('figtht', 4);     -- 27
INSERT INTO `Subcategory` (name, category_id) VALUES ('car', 4);        -- 28
-- culture : 8
INSERT INTO `Subcategory` (name, category_id) VALUES ('book', 8);       -- 13
INSERT INTO `Subcategory` (name, category_id) VALUES ('cinema', 8);     -- 14
INSERT INTO `Subcategory` (name, category_id) VALUES ('food', 8);       -- 15
INSERT INTO `Subcategory` (name, category_id) VALUES ('music', 8);      -- 16
-- societal : 11
INSERT INTO `Subcategory` (name, category_id) VALUES ('family', 11);    -- 17
INSERT INTO `Subcategory` (name, category_id) VALUES ('human rights', 11); -- 18
INSERT INTO `Subcategory` (name, category_id) VALUES ('politics', 11);   -- 19
INSERT INTO `Subcategory` (name, category_id) VALUES ('epidemic', 11);   -- 20
INSERT INTO `Subcategory` (name, category_id) VALUES ('celebration', 11);-- 21
INSERT INTO `Subcategory` (name, category_id) VALUES ('companies', 11);  -- 23
INSERT INTO `Subcategory` (name, category_id) VALUES ('finance', 11);    -- 24


INSERT INTO `Source` (name) VALUES ('News');
INSERT INTO `Source` (name) VALUES ('Video');
INSERT INTO `Source` (name) VALUES ('Social Network');
INSERT INTO `Source` (name) VALUES ('Image');
INSERT INTO `Source` (name) VALUES ('Ads');

INSERT INTO `News_Type` (name) VALUES ('text');
INSERT INTO `News_Type` (name) VALUES ('image');
INSERT INTO `News_Type` (name) VALUES ('youtube_video');
INSERT INTO `News_Type` (name) VALUES ('other_video');