import requests
import json
import sqlite3

conn = sqlite3.connect("../../slot-machine.db")
db = conn.cursor()

subjects = ["ski", "minecraft", "pewdiepie", "dr nozman"]
categories = [4, 1, 1, 3]

youtube_token = "<TOKEN>"
headers = {'Authorization': 'Bearer ' + youtube_token}

for i in range(0, len(subjects)):
    url = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q="+ subjects[i]
    response = requests.get(url, headers=headers)
    news_list = response.json()

    for spnippet in news_list["items"]:
        news = spnippet["snippet"]
        if ("videoId" in spnippet["id"]):
            row = {
                    "id": None,
                    "title": news["title"],
                    "url": "https://www.youtube.com/watch?v="+ spnippet["id"]["videoId"],
                    "url_image": news["thumbnails"]["default"]["url"],
                    "content": news["description"],
                    "source_id": 2,
                    "category_id": categories[i],
                    "type_id": 3,
                    "author": None
                    }
            db.execute("INSERT INTO News VALUES (:id, :title, :url, :url_image, :content, :source_id, :category_id, :type_id, :author)", row)
conn.commit()