import requests
import sqlite3

conn = sqlite3.connect("../../slot-machine.db")
db = conn.cursor()

subjects = ["science", "health", "sport", "cat", "tennis"]
categories = [3, 2, 4, 1, 4]

apiKey = "<API_KEY>"

for i in range(0, len(subjects)):
        response = requests.get("http://newsapi.org/v2/everything?q=" + subjects[i] + "&apiKey=" + apiKey)
        news_list = response.json()

        for news in news_list["articles"]:
            row = {
                    "id": None,
                    "title": news["title"],
                    "url": news["url"],
                    "url_image": news["urlToImage"],
                    "content": news["description"],
                    "source_id": 1,
                    "category_id": categories[i],
                    "type_id": 1,
                    "author": None
                    }
            db.execute("INSERT INTO News VALUES (:id, :title, :url, :url_image, :content, :source_id, :category_id, :type_id, :author)", row)

conn.commit()