import twint
import sqlite3

conn = sqlite3.connect("../../slot-machine.db")
db = conn.cursor()

subjects = ["Trump", "machine learning", "bio", "dog"]
categories = [9, 3, 2, 1]

config = twint.Config()
config.Lang = "en"
config.Store_object_tweets_list = True
config.Store_object = True
config.Pandas = True
config.Limit = 20

for i in range(0, len(subjects)):
    config.Search = subjects[i]
    twint.run.Search(config)

    for tweet in twint.output.tweets_list:
        row = {
            "id": None,
            "title": "",
            "url": tweet.link,
            "url_image": tweet.photos[0] if len(tweet.photos) else "",
            "content": tweet.tweet,
            "source_id": 3,
            "category_id": categories[i],
            "type_id": 1,
            "author": tweet.username
        }
        db.execute("INSERT INTO News VALUES (:id, :title, :url, :url_image, :content, :source_id, :category_id, :type_id, :author)", row)
conn.commit() 