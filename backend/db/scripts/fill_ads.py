import requests
import sqlite3

conn = sqlite3.connect("../../slot-machine.db")
db = conn.cursor()
newsToInsert = []

rows = [{
        "id": None,
        "title": "MISS DIOR – The new Eau de Parfum",
        "url": "https://www.youtube.com/watch?v=h4s0llOpKrU",
        "url_image": "",
        "content": "AND YOU, WHAT WOULD YOU DO FOR LOVE? \nWatch the new Miss Dior film starring Natalie Portman, directed by Emmanuel Cossu.\n#missdiorforlove",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "Clash Royale: The Last Second (Official Commercial)",
        "url": "https://www.youtube.com/watch?v=TJryIc0Cwy4",
        "url_image": "",
        "content": """When it comes down to the last second... 

        Subscribe! http://supr.cl/SubRoyale""",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "McDonald's Service à Table",
        "url": "https://www.youtube.com/watch?v=UV2dnawkf0I",
        "url_image": "",
        "content": "",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "Copie TV Coca-Cola zero sucres (60 sec)",
        "url": "https://www.youtube.com/watch?v=_eC5MmIPbi8",
        "url_image": "",
        "content": "Vivez sans limites avec Coca-Cola zero sucres",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "Campagne 2018 - 30 Millions d'Amis",
        "url": "https://www.youtube.com/watch?v=XW5-CQdmE_8",
        "url_image": "",
        "content": "Le film de la Fondation 30 Millions d'Amis, réalisé par Xavier Giannoli, met en scène l’histoire de la relation exceptionnelle qui s’établit entre maître et chien. Dites #NONALABANDON : https://goo.gl/LRUSdB",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "Cetelem - Publicité - Parfum",
        "url": "https://www.youtube.com/watch?v=mVdvTivLAAs",
        "url_image": "",
        "content": "",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "FIAT Nouvelle 500X - Retour vers le futur",
        "url": "https://www.youtube.com/watch?v=Upch85La3Rc",
        "url_image": "",
        "content": "Faites un bon dans le temps avec la nouvelle Fiat 500X bientôt dans notre showroom Fiat Nice.",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "FIAT Nouvelle 500X - Retour vers le futur",
        "url": "https://www.youtube.com/watch?v=Upch85La3Rc",
        "url_image": "",
        "content": "Faites un bon dans le temps avec la nouvelle Fiat 500X bientôt dans notre showroom Fiat Nice.",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "OREO - OWEN WILSON @ Le parc",
        "url": "https://www.youtube.com/watch?v=LzEsOw-_CGs",
        "url_image": "",
        "content": "Owen Wilson présente (à sa manière) les biscuits OREO goût beurre de cacahuète ! Et vous DO YOU SPEAK OREO ?",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "Amazon Echo - Nouveau Spot Commercial - Claire (30s)",
        "url": "https://www.youtube.com/watch?v=ZChAcuTPqvs",
        "url_image": "",
        "content": "Amazon Echo, à vos côtés pour tous les petits moments de la vie... et les grands aussi.",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
},
{
        "id": None,
        "title": "CHEZ TATI T'AS TOUT !",
        "url": "https://www.youtube.com/watch?v=qU1jZ7LqlgA",
        "url_image": "",
        "content": "Découvrez la nouvelle collection de notre univers de la maison pour décorer votre intérieur à prix Tati !",
        "source_id": 2,
        "category_id": 1,
        "type_id": 3,
        "author": None,
        "fake": None
}]

db.executemany("INSERT INTO News VALUES (:id, :title, :url, :url_image, :content, :source_id, :category_id, :type_id, :author, :fake)", rows)

conn.commit()





