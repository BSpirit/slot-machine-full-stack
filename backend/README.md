# slot-machine-backend

## Project setup

1. Handle environnement variable:
```
echo "export APP_SETTINGS=development" > .env
source .env
```
*NB: "APP_SETTINGS" environnement variable should be set to "production" in a production environnement.*


2. Create virtual environnement:
```
source venv/bin/activate
```

3. Use virtual environnement:
```
source venv/bin/activate
```

4. Install requirements in virtual environnement:
```
pip install -r requirements.txt
```

5. Install Gunicorn
```
pip install gunicorn
```

6. Create database:
```
cd db
sqlite3 slot-machine.db < schema.sql
```


## Useful commands

Run api using gunicorn
```
gunicorn -w 4 app:server --bind 0:8000
```
*NB: API listens on port 8000.*

Stop using virtual environnement:
```
deactivate
```

Update schema.sql file from DB:
```
cd db
sqlite3 slot-machine.db .dump > schema.sql
```
