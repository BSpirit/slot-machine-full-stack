import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """Parent configuration class."""
    CSRF_ENABLED = True
    SECRET = 's3cr3t'
    BUNDLE_ERRORS = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DATABASE_FILEPATH = 'db/slot-machine.db'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, DATABASE_FILEPATH)
    

class DevelopmentConfig(Config):
    """Configurations for Development."""
    DEBUG = True


class ProductionConfig(Config):
    """Configurations for Production."""
    DEBUG = False
    TESTING = False


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
}
