import os
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from config import app_config
from dashboard.dashboard import Dashboard

config = app_config[os.getenv('APP_SETTINGS')]
server = Flask(__name__)
Dashboard(server, config.DATABASE_FILEPATH)
CORS(server)
server.config.from_object(config)
db = SQLAlchemy(server)
api = Api(server)



from api.resources.source import SourceListAPI, SourceAPI
from api.resources.category import CategoryListAPI, CategoryAPI
from api.resources.subcategory import SubcategoryListAPI, SubcategoryAPI
from api.resources.news_type import NewsTypeListAPI, NewsTypeAPI
from api.resources.user import UserListAPI, UserAPI
from api.resources.news import NewsListAPI, NewsAPI
from api.resources.game import GameListAPI, GameAPI


api.add_resource(SourceListAPI, '/sources')
api.add_resource(SourceAPI, '/sources/<int:id>')
api.add_resource(CategoryListAPI, '/categories')
api.add_resource(CategoryAPI, '/categories/<int:id>')
api.add_resource(SubcategoryListAPI, '/subcategories')
api.add_resource(SubcategoryAPI, '/subcategories/<int:id>')
api.add_resource(NewsTypeListAPI, '/news-types')
api.add_resource(NewsTypeAPI, '/news-types/<int:id>')
api.add_resource(UserListAPI, '/users')
api.add_resource(UserAPI, '/users/<int:id>')
api.add_resource(NewsListAPI, '/news')
api.add_resource(NewsAPI, '/news/<int:id>')
api.add_resource(GameListAPI, '/games')
api.add_resource(GameAPI, '/games/<int:id>')


import api.routes
