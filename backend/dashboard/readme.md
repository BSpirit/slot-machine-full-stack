# Dashboard

This part is to visualize data. We display all data after user "played" with our slot machine.
We show how long they pass on our application, how long on each category and what categories are more liked.

### Prerequisites

You must have python, and install requirements.

```
pip install -r requirements.txt
```

## Getting Started

To start the dashboard, you can type `python3 dashboard.py <database_file>`.
It will display on localhost:8050 several graphs showing what I described above.

## How it works ?

This soft gets data from [database](../db/readme.md) and formats data.
Data is handled to display graphs.
Moreover, we refresh data every 30s, and when the user finish to read/watch a random content.