import sqlite3
import sys

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from math import floor


class Dashboard:
    slice_age = {'18-24': 0, '25-34': 0, '35-44': 0, '45-54': 0, '55-64': 0, '65-90': 0}

    def __init__(self, server, dbname):
        self.dbname = dbname
        self.app = dash.Dash(__name__, server=server, url_base_pathname='/dashboard/')
        self.app.scripts.config.serve_locally = True
        self.app.css.config.serve_locally = True
        self.colors = {
            'background': 'black',
            'text': '#7FDBFF'
        }

        self.minutes = 0
        self.secondes = 0
        self.likes = 0

        conn = sqlite3.connect(self.dbname)
        conn.row_factory = self.dict_factory
        self.db = conn.cursor()

        self.user_id = self.getUserId()
        self.categoryColor = self.getCategoryColors()

        self.app.layout = html.Div(style={'backgroundColor': self.colors['background']}, children=[
            html.Header(
                html.H1(
                    children='Slot Machine',
                    style={
                        'textAlign': 'center',
                        'color': self.colors['text']
                    }
                )
            ),
            html.Div(
                [
                    html.Div(
                        html.H3("Time average on slot machine"),
                        className='title'
                    ),
                    html.Div(dcc.Graph(id='time-per-age', animate=True)),
                    html.Div(children=[
                        # html.P("Your time: "),
                        html.H3(id='user-time')
                    ]),
                    html.Div(dcc.Graph(id='time-per-gender', animate=True)),

                    html.Div(
                        html.H3("Time average per category"),
                        className='title'
                    ),
                    html.Div(dcc.Graph(id='time-per-category-age', animate=True)),
                    html.Div(children=[
                        html.P("Your time :"),
                        html.H3(html.Ul(id='user-time2'))
                    ]),
                    html.Div(dcc.Graph(id='time-per-category-gender', animate=True)),

                    # html.Div(
                    #     html.H3("Likes per category"),
                    #     className='title'
                    # ),
                    # html.Div(dcc.Graph(id='like-per-age', animate=True)),
                    # html.Div(children=[
                    #     html.P("Your likes per category :"),
                    #     html.H3(html.Ul(id='user-likes'))
                    # ]),
                    # html.Div(dcc.Graph(id='like-per-category', animate=True)),
                ],
                className='grid'
            ),

            dcc.Interval(id='graph-update', interval=10 * 1000),
        ])
        self.app.callback([Output('time-per-gender', 'figure'),
                           Output('user-time', 'children'),
                           Output('time-per-age', 'figure'),
                           Output('time-per-category-gender', 'figure'),
                           Output('user-time2', 'children'),
                           Output('time-per-category-age', 'figure'),
                           # Output('like-per-category', 'figure'),
                           # Output('user-likes', 'children'),
                           # Output('like-per-age', 'figure')
                           ],
                          [Input('graph-update', 'n_intervals')])(self.update_graph_scatter)

    def getUserId(self):
        self.db.execute("SELECT id FROM `User` ORDER BY id DESC LIMIT 1;")
        result = self.db.fetchone()
        return result["id"] if result else 0

    def getCategoryColors(self):
        color = {}
        self.db.execute("SELECT name, color FROM `Category`;")
        results = self.db.fetchall()
        for res in results:
            color[res['name']] = res['color']
        return color

    def getAverageTimeGender(self, results):
        res = {"man": 0, "woman": 0, "other": 0}
        man = 0
        woman = 0
        other = 0

        for result in results:
            if (result["sexe"] == "M"):
                res["man"] += result["passed_time"]
                man += 1
            elif (result["sexe"] == "F"):
                res["woman"] += result["passed_time"]
                woman += 1
            else:
                res["other"] += result["passed_time"]
                other += 1

        if man >= 1:
            res["man"] /= man
        if woman >= 1:
            res["woman"] /= woman
        if other >= 1:
            res["other"] /= other
        return res

    def getAverageTimeAge(self, results):
        res = self.slice_age.copy()
        count = self.slice_age.copy()

        for result in results:
            for age in res:
                start = int(age.split("-")[0])
                end = int(age.split("-")[1])
                if start <= result["age"] <= end:
                    res[age] += result["passed_time"]
                    count[age] += 1
                    break

        for age in res:
            if count[age] >= 1:
                res[age] /= count[age]
        return res

    def getUserInfo(self, results):
        total = 0
        for result in results:
            if result["id"] == self.user_id:
                total += result["passed_time"]
                self.likes += result["liked"]

        self.minutes = floor(total / 60)
        self.secondes = total % 60

    def getAverageTimePerCategoryGender(self, results):
        res = {"man": {}, "woman": {}, "other": {}}
        man = {}
        woman = {}
        other = {}

        # Get data
        for result in results:
            if (result["sexe"] == "M"):
                if (result["category"] in man):
                    man[result["category"]] += result["passed_time"]
                    man[result["category"] + "_count"] += 1
                else:
                    man[result["category"]] = result["passed_time"]
                    man[result["category"] + "_count"] = 1
            elif (result["sexe"] == "F"):
                if (result["category"] in woman):
                    woman[result["category"]] += result["passed_time"]
                    woman[result["category"] + "_count"] += 1
                else:
                    woman[result["category"]] = result["passed_time"]
                    woman[result["category"] + "_count"] = 1
            else:
                if (result["category"] in other):
                    other[result["category"]] += result["passed_time"]
                    other[result["category"] + "_count"] += 1
                else:
                    other[result["category"]] = result["passed_time"]
                    other[result["category"] + "_count"] = 1


        # Calculate average
        for key in man:
            if "_count" in key:
                continue
            man[key] /= man[key + "_count"]
            res["man"][key] = man[key]

        for key in woman:
            if "_count" in key:
                continue
            woman[key] /= woman[key + "_count"]
            res["woman"][key] = woman[key]

        for key in other:
            if "_count" in key:
                continue
            other[key] /= other[key + "_count"]
            res["other"][key] = other[key]

        return res

    def getTimePerCategoryUser(self, results):
        res = {}

        for result in results:
            if (result["category"] in res and result["id"] == self.user_id):
                res[result["category"]] += result["passed_time"]
            elif (result["category"] not in res and result["id"] == self.user_id):
                res[result["category"]] = result["passed_time"]
            elif (result["category"] not in res and result["id"] != self.user_id):
                res[result["category"]] = 0

        items = []
        for key in res:
            minutes = floor(res[key] / 60)
            secondes = res[key] % 60
            # items.append(html.Li(key + ": " + str(minutes) + ":" + str(secondes), style={'color': self.categoryColor[key]}))
            items.append(html.Li(str(minutes) + ":" + str(secondes), style={'color': self.categoryColor[key]}))
        return items

    def getAverageTimePerCategoryAge(self, results):
        res = {}
        count = {}

        # Get data
        for result in results:
            # age = result["age"]
            category = result["category"]
            if category in res:
                for age in res[category]:
                    start = int(age.split("-")[0])
                    end = int(age.split("-")[1])
                    if start <= result["age"] <= end:
                        res[category][age] += result["passed_time"]
                        count[category][age] += 1
                        break
            else:
                res[category] = self.slice_age.copy()
                count[category] = self.slice_age.copy()

        for category_key in res:
            for age in res[category_key]:
                if count[category_key][age] >= 1:
                    res[category_key][age] /= count[category_key][age]

        result = []
        for key in res:
            result.append({'x': list(res[key].keys()), 'y': list(res[key].values()), 'type': 'bar', 'name': key,
                           'marker': dict(color=self.categoryColor[key])})

        return result

    def getLikesPerCategoryGender(self, results):
        res = {"man": {}, "woman": {}, "other": {}}

        for result in results:
            if (result["sexe"] == "M"):
                if (result["category"] in res["man"]):
                    res["man"][result["category"]] += result["liked"]
                else:
                    res["man"][result["category"]] = result["liked"]
            elif (result["sexe"] == "F"):
                if (result["category"] in res["woman"]):
                    res["woman"][result["category"]] += result["liked"]
                else:
                    res["woman"][result["category"]] = result["liked"]
            else:
                if (result["category"] in res["other"]):
                    res["other"][result["category"]] += result["liked"]
                else:
                    res["other"][result["category"]] = result["liked"]
        return res

    def getLikesPerCategoryUser(self, results):
        res = {}

        for result in results:
            if (result["category"] in res and result["id"] == self.user_id):
                res[result["category"]] += result["liked"]
            elif (result["category"] not in res and result["id"] == self.user_id):
                res[result["category"]] = result["liked"]
            elif (result["category"] not in res and result["id"] != self.user_id):
                res[result["category"]] = 0

        items = []
        for key in res:
            items.append(html.Li(key + ": " + str(res[key]), style={'color': self.categoryColor[key]}))
        return items

    def getLikesPerCategoryAge(self, results):
        res = {}

        # Get data
        for result in results:
            age = result["age"]
            category = result["category"]
            if (category in res):
                for age in res[category]:
                    start = int(age.split("-")[0])
                    end = int(age.split("-")[1])
                    if start <= result["age"] <= end:
                        res[category][age] += result["liked"]
                        break
            else:
                res[category] = self.slice_age.copy()

        result = []
        for key in res:
            result.append({'x': list(res[key].keys()), 'y': list(res[key].values()), 'type': 'bar', 'name': key,
                           'marker': dict(color=self.categoryColor[key])})

        return result

    def getResults(self, db):
        db.execute("SELECT G.passed_time, G.liked, U.sexe, U.age, U.id, C.name as category, S.name as source "
                        + "FROM `Game` as G "
                        + "INNER JOIN User as U on G.user_id = U.id "
                        + "INNER JOIN News as N on G.news_id = N.id "
                        + "INNER JOIN Category as C on N.category_id = C.id "
                        + "INNER JOIN Source as S on N.source_id = S.id;")
        return db.fetchall()

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def update_graph_scatter(self, input_data):
        self.user_id = self.getUserId()
        conn = sqlite3.connect(self.dbname)
        conn.row_factory = self.dict_factory
        db = conn.cursor()

        blue = dict(color='rgb(35, 52, 146)')
        pink = dict(color='rgb(211, 52, 127)')
        yellow = dict(color='rgb(249, 199, 0)')

        results = self.getResults(db)
        self.getUserInfo(results)

        userInfo = str(self.minutes) + ":" + str(self.secondes)

        averageTimeGender = self.getAverageTimeGender(results)
        averageTimeAge = self.getAverageTimeAge(results)

        averageTimePerCategoryGender = self.getAverageTimePerCategoryGender(results)
        timePerCategoryUser = self.getTimePerCategoryUser(results)
        averageTimePerCategoryAge = self.getAverageTimePerCategoryAge(results)

        # likesPerCategoryGender = self.getLikesPerCategoryGender(results)
        # likesPerCategoryUser = self.getLikesPerCategoryUser(results)
        # likesPerCategoryAge = self.getLikesPerCategoryAge(results)

        timePerGender = {'data': [
            {'x': ['man'], 'y': [averageTimeGender["man"]], 'type': 'bar', 'name': 'man', 'marker': blue},
            {'x': ['woman'], 'y': [averageTimeGender["woman"]], 'type': 'bar', 'name': 'woman', 'marker': pink},
            {'x': ['other'], 'y': [averageTimeGender["other"]], 'type': 'bar', 'name': 'other', 'marker': yellow},
        ],
            'layout': {
                'title': 'Time average per gender',
                'plot_bgcolor': self.colors['background'],
                'paper_bgcolor': self.colors['background'],
                'font': {
                    'color': self.colors['text']
                },
                'yaxis': {
                    'title': 'Time is seconds'
                }
            }
        }

        timePerAge = {'data': [
            {'x': list(averageTimeAge.keys()), 'y': list(averageTimeAge.values()), 'type': 'bar', 'name': 'age'},
        ],
            'layout': {
                'title': 'Time average per age',
                'plot_bgcolor': self.colors['background'],
                'paper_bgcolor': self.colors['background'],
                'font': {
                    'color': self.colors['text']
                },
                'yaxis': {
                    'title': 'Time is seconds'
                }
            }
        }

        timeCategoryPerGender = {'data': [
            {'x': list(averageTimePerCategoryGender["man"].keys()),
             'y': list(averageTimePerCategoryGender["man"].values()), 'type': 'bar', 'name': 'man', 'marker': blue},
            {'x': list(averageTimePerCategoryGender["woman"].keys()),
             'y': list(averageTimePerCategoryGender["woman"].values()), 'type': 'bar', 'name': 'woman', 'marker': pink},
            {'x': list(averageTimePerCategoryGender["other"].keys()),
             'y': list(averageTimePerCategoryGender["other"].values()), 'type': 'bar', 'name': 'other', 'marker': yellow},
        ],
            'layout': {
                'title': 'Time average per category per gender',
                'plot_bgcolor': self.colors['background'],
                'paper_bgcolor': self.colors['background'],
                'font': {
                    'color': self.colors['text']
                },
                'yaxis': {
                    'title': 'Time is seconds'
                }
            }
        }

        timeCategoryPerAge = {'data': averageTimePerCategoryAge,
                              'layout': {
                                  'title': 'Time average per category per age',
                                  'plot_bgcolor': self.colors['background'],
                                  'paper_bgcolor': self.colors['background'],
                                  'font': {
                                      'color': self.colors['text']
                                  },
                                'yaxis': {
                                    'title': 'Time is seconds'
                                }
                              }
                              }

        # likeCategoryGender = {'data': [
        #     {'x': list(likesPerCategoryGender["man"].keys()), 'y': list(likesPerCategoryGender["man"].values()),
        #      'type': 'bar', 'name': 'man', 'marker': blue},
        #     {'x': list(likesPerCategoryGender["woman"].keys()), 'y': list(likesPerCategoryGender["woman"].values()),
        #      'type': 'bar', 'name': 'woman', 'marker': pink},
        #     {'x': list(likesPerCategoryGender["other"].keys()), 'y': list(likesPerCategoryGender["other"].values()),
        #      'type': 'bar', 'name': 'other', 'marker': yellow},
        # ],
        #     'layout': {
        #         'title': 'Likes per category per gender',
        #         'plot_bgcolor': self.colors['background'],
        #         'paper_bgcolor': self.colors['background'],
        #         'font': {
        #             'color': self.colors['text']
        #         }
        #     }
        # }
        #
        # likeCategoryAge = {'data': likesPerCategoryAge,
        #                    'layout': {
        #                        'title': 'Likes per category per age',
        #                        'plot_bgcolor': self.colors['background'],
        #                        'paper_bgcolor': self.colors['background'],
        #                        'font': {
        #                            'color': self.colors['text']
        #                        }
        #                    }
        #                    }

        return timePerGender, html.H3(userInfo), timePerAge, \
               timeCategoryPerGender, timePerCategoryUser, timeCategoryPerAge #, \
               # likeCategoryGender, likesPerCategoryUser, likeCategoryAge
