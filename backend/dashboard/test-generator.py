import sqlite3
import random

conn = sqlite3.connect("dev-test.db")
db = conn.cursor()

# Create 3 users
for x in range(7):
    sexe = random.choice(["F", "M"])
    age = random.randint(16, 60)
    user = {"id": None, "sexe": sexe, "age": age}
    db.execute("INSERT INTO User VALUES (:id, :sexe, :age)", user)
    userId = db.lastrowid

    #Foreach user, create 1..3 games
    gameCount = random.randint(1, 3)
    for i in range(gameCount):
        game =  {
            "id": None,
            "news_id" : random.randint(1,257),
            "user_id": userId,
            "passed_time": random.randint(30,300),
            "liked": random.randint(0,1)
        }
        db.execute("INSERT INTO Game VALUES (:id, :news_id, :user_id, :passed_time, :liked)", game)

conn.commit() 