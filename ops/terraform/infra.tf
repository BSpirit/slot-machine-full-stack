provider "aws" {
  region  = "us-east-1"
  version = "~> 2.43"
}

resource "aws_key_pair" "app_key_pair" {
  key_name   = "app_key"
  public_key = file("~/.ssh/devops_id_rsa.pub")
}

resource "aws_vpc" "app_vpc" {
  cidr_block = "192.168.0.0/16"

  tags = {
    Name = "app"
  }
}

resource "aws_subnet" "app_subnet" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "192.168.1.0/24"

  tags = {
    Name = "app"
  }
}

resource "aws_internet_gateway" "app_ig" {
  vpc_id = aws_vpc.app_vpc.id

  tags = {
    Name = "app"
  }
}

resource "aws_route" "app_rt" {
    route_table_id = aws_vpc.app_vpc.main_route_table_id
    gateway_id = aws_internet_gateway.app_ig.id
    destination_cidr_block = "0.0.0.0/0"
}

resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http"
  description = "Allow SSH and HTTP inbound traffic"
  vpc_id      = aws_vpc.app_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "app"
  }
}

resource "aws_instance" "app" {
  ami                    = "ami-04b9e92b5572fa0d1"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.app_key_pair.key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh_http.id]
  subnet_id              = aws_subnet.app_subnet.id
  associate_public_ip_address = "true"

  tags = {
    Name = "app"
  }
}

output "ip" {
  value = aws_instance.app.public_ip
}
