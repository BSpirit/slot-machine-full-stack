# Slot machine - ops

A EC2 AWS instance will be used to deploy our app.

## Terraform

Terraform is used to create the AWS EC2 instance and make it accessible from outside using a public IP address.
To authenticate, Terraform needs AWS credentials. They must be copied and pasted to `~/.aws/credentials` file.

**WARNING** : 

1. Initialization
```
terraform init
```

2. Validate changes
```
terraform apply
```
**NB** : Use `terraform destroy` to free all ressources created in AWS.

## Ansible

Once the EC2 instance is up, Ansible is used to deploy the app code.

Ansible will not compress the projects by itself, `back.tar.bz2` and `front.tar.bz2`files shoud be 
